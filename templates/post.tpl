<div id="header">
    <h1><a href="{{BASEURL}}">{{SITENAME}}</a>  <span id='miniHead'></span></h1>
</div>
<div id="posts">
    <a href="{{BASEURL}}"><-- Go Back</a>
    <div id="post_header">
        <h2><a href="{{POSTURL}}">{{TITLE}}</a></h2>
    </div>
    <div id="post_details">
        <p><b>{{AUTHOR}}</b> on {{DATE}} in {{CATEGORIES}} about <i>{{TAGS}}</i></p>
    </div>
    <div id="post_content">
        <p>{{CONTENT}}</p>
    </div>
</div>
