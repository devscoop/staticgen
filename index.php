<?php
    ob_start();
    session_start();
    require_once 'admin/post.inc.php';
    require_once 'admin/template.inc.php';
    require_once 'admin/config.php';
    $posts = new Post();
    $posts->init();
    $template = new Template();
    $template->html_header($SITE_NAME." - ".$SITE_DESC, $SITE_KEYWORDS,array(),array("css/default.css"),$SITE_NAME." - ".$SITE_DESC);
    if(isset($_SESSION['username']) && !empty($_SESSION['username'])) {
        
        ?>
            <div><a href="/admin">View Dashboard</a></div>
        <?
    }
    $template->header();
    
    $posts->retrievePosts($i);
    
    
    $template->sidebar();
    
    $template->html_footer();
    ob_flush();
?>
