# STATICGEN

## What is this?
staticgen is a PHP implementation of a static site generator. It converts posts stored in XML format into .html pages.

## How do I use it?
1. Open admin/config.php and set up the defaults
2. Upload the files to your server
3. Open /admin in your browser
4. Login 
5. Add Posts, Manage Posts, Add Media files, Manage Media Files
6. Generate Static Pages

## Why?
Reading and writing to XML is seriously fast. And if you use shared hosting for personal blogs and small online notebooks, a database server can wreck your rythym. 

## Features
1. Adding and managing posts
2. Adding and managing media files
3. Generating .html files, a sitemap and a rss feed

