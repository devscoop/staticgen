<?php


$SITE_NAME      = "WheresTheHash?";
$SITE_DESC      = "home of staticgen and more..";
$SITE_KEYWORDS  = "staticgen, mangafox parser, python, php, javascript, c++, programming";
$SITE_BASE_URL  = "http://www.wheresthehash.com/";
$ADMIN_USERNAME = "###";
$ADMIN_PASSWORD = "###";
$XML_SOURCE     = $SITE_BASE_URL."sources/posts.xml";  
$RELATIVE_XML_SOURCE = "../sources/posts.xml";
$UPLOAD_DIR = "media";
$DEFAULT_TEMPLATE = "templates/post.tpl";
$DEFAULT_TIMEZONE = "Australia/Melbourne";
?>
