<?php
require_once 'config.php';
date_default_timezone_set($DEFAULT_TIMEZONE);


class Post {
    
    private $xml;
    
    public function init() {
        global $XML_SOURCE;
        $xml       = simplexml_load_file($XML_SOURCE);
        $this->xml = $xml;
    
        
    }
    
    public function returnStream() {
        return $this->xml;
    }
    
    public function sanitise($word) {
        
        $word = stripslashes($word);
        $word = htmlspecialchars(utf8_encode($word));
        $word = str_replace("Â","",$word);
        return $word;
        
    }
    
    public function addPost($title,$category,$tags,$content,$author,$slug,$page) {
        global $XML_SOURCE,$RELATIVE_XML_SOURCE;
        $count = 0;
        foreach($this->xml->post as $post) {
            $count++;
        }
        $last_id = $this->xml->post[$count-1]->id;
        $new_id = $last_id+1;
        $new_post = $this->xml->addChild('post');
        $new_post->addChild('id',$new_id);
        $new_post->addChild('date',date('Y/m/d H:i:s'));
        $new_post->addChild('title',$this->sanitise($title));
        $new_post->addChild('category',$this->sanitise($category));
        $new_post->addChild('tags',$this->sanitise($tags));
        $new_post->addChild('content',$this->sanitise($content));
        $new_post->addChild('author',$this->sanitise($author));
        $new_post->addChild('slug',$this->sanitise($slug));
        $new_post->addChild('page',$this->sanitise($page));
        $this->xml->asXML($RELATIVE_XML_SOURCE);
    
        return $new_id;
    }
    
    public function editPost($title,$category,$tags,$content,$slug,$page,$id) {
       global $RELATIVE_XML_SOURCE;
        for($i=0;$i<$id;$i++) {
            if($this->xml->post[$i]->id == $id) {
                $this->xml->post[$i]->title = $this->sanitise($title);
                $this->xml->post[$i]->category = $this->sanitise($category);
                $this->xml->post[$i]->tags = $this->sanitise($tags);
                $this->xml->post[$i]->content = $this->sanitise($content);
                $this->xml->post[$i]->slug = $this->sanitise($slug);
                $this->xml->post[$i]->page = $this->sanitise($page);
                $this->xml->post[$i]->date = date('Y/m/d H:i:s');
            }
        }
        $this->xml->asXML($RELATIVE_XML_SOURCE);
        return $id;

    }
    
    public function deletePost($id) {
       global $RELATIVE_XML_SOURCE;
        for($i=0;$i<$id;$i++) {
            if($this->xml->post[$i]->id == $id) {
                unset($this->xml->post[$i]);
            }
        }
        $this->xml->asXML($RELATIVE_XML_SOURCE);
        

    }
    
    public function renderNotFound() {
        echo "<h2>There's nothing to display here!</h2>";
        echo "<p>Check back later to see if the owner has added something awesome!</p>";
    }
    
    public function retrievePost($id) {
        $displayed = False;
        foreach ($this->xml->post as $post) {
            if($post->id == $id) {
                $displayed = True;
                return $post;
            }
        }
        
        if($displayed == False) {
            return False;
        }
        
    }
    
    public function retrievePosts() {
        global $SITE_BASE_URL;
        
        $displayed = False;
        $posts = array();
        $i = 0;
        echo "<div id='posts'>";
        foreach ($this->xml->post as $post) {
                
                    if($post->page == 0) {
                        $displayed = True;
                        $date      = date('d/m/Y',strtotime($post->date));
                        $posts[$date.$i] ="<div id='post'>";
                        $posts[$date.$i].="<div id='postHeader'><div id='postDate'>".$date."</div><div id='postTitle'><a href='".$SITE_BASE_URL.$this->getSlug($i)."'>".$post->title."</a></div></div>";
                        $posts[$date.$i].="<div id='info'>".$this->xml->post[$i]->author." on ".$post->category."</div>";
                        $posts[$date.$i].="</div>";
                    }
                $i+=1;
        }
        
        if($displayed == False) {
            $this->renderNotFound();
        }
        asort($posts);
        foreach ($posts as $p) {
            echo $p;
        }
        
        echo "</div>";

    }
    
    
    public function retrievePages() {
        global $SITE_BASE_URL;
        
        $displayed = False;
        $posts = array();
        $i = 0;
        
        foreach ($this->xml->post as $post) {
                
                    if($post->page == 1) {
                        $displayed = True;
                        
                         $pages[] = "<div id='pageTitle'><a href='".$SITE_BASE_URL.$this->getSlug($i)."'>".$post->title."</a></div>";

                    }
                $i+=1;
        }
        if(count($pages)>0) {
		echo "<div id='pages'>";
		echo "<h3>Pages</h3>";
		foreach ($pages as $page) {
			echo $page;
		}
		echo "</div>";
	}
        
        

    }    
    
    
    public function getSlug($id) {
        
       
        $slug = $this->xml->post[$id]->slug;
        $file = $slug.".html";
        return $file;
        
    }
    
    public function getCategories() {
        
        $categories = array();
        foreach ($this->xml->post as $posts) {
            $cat = (string)($posts->category);
            if(!in_array($cat,$categories)) {
                $categories[] = $cat;
            }
        }
        
        return $categories;
    }
    
    
}

?>
