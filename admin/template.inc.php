<?php
require_once 'config.php';
require_once 'post.inc.php';
$posts = new Post();
class Template {
    
    public function header() {
        global $SITE_NAME,$SITE_BASE_URL,$SITE_DESC;
        echo "<div id='header'><h1><a href='".$SITE_BASE_URL."'>".$SITE_NAME."</a> - <span id='miniHead'>".$SITE_DESC."</span></h1></div>";
    }
    
    
    
    public function sidebar() {
        global $DEFAULT_POSTS,$posts;
        $post = new Post();
        $post->init();
        echo "<div id='sidebar'>";
            
            $post->retrievePages();

            echo "<h3>Links</h3>";
            echo "<div id='sidebarlinks'>";
                echo "<a href='https://github.com/thehash'>Github</a>";
            echo "</div>";
            echo "<div id='sidebarlinks'>";
                echo "<a href='feed.xml'>RSS Feed</a>";
            echo "</div>";
            
            
            
       echo "</div>";
        
        
    }
    
    public function html_header($meta_desc,$meta_keys,$scripts,$csss,$title) {
        
        global $SITE_BASE_URL;
       ?>
                
                <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
                <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="description" content="<?php echo $meta_desc; ?>" />
                <meta name="keywords" content="<?php echo $meta_keys; ?>" />
                <title><?php echo $title; ?></title>

       <?
                foreach($scripts as $script) {
                    ?>
                <script type="text/javascript" src="<?php echo $SITE_BASE_URL.$script; ?>" ></script>
                    <?
                }
                
                foreach($csss as $css) {
                    ?>
                        <link rel="stylesheet" href="<?php echo $SITE_BASE_URL.$css; ?>" type="text/css" />
                    <?
                }
        
        
    }
    
    public function html_footer() {
         global $SITE_NAME,$SITE_BASE_URL;
        $SITE_FOOTER   = "&copy; 2012 - <a href='".$SITE_BASE_URL."'>".$SITE_NAME."</a> - <a href='sitemap.html'>Sitemap</a>";
        echo "<div id='footer'>".$SITE_FOOTER."</div>";
        echo "</body></html>";      
        
    }
}

?>
