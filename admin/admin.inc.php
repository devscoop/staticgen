<?php

require_once 'config.php';
require_once 'post.inc.php';
require_once 'generator.inc.php';

class Admin {
   private $post; 
   
   public function login($u,$p) {
       global $ADMIN_PASSWORD, $ADMIN_USERNAME; 
       
       if($u == $ADMIN_USERNAME && $p == $ADMIN_PASSWORD) {
                $_SESSION['username'] = $u;
                
                $this->renderDashboard();
       }
       else {
           header("Location:index.php");
       }
    }
    
    public function renderDashboard() {
        global $SITE_BASE_URL;
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])) {
            session_destroy();
            header("Location:index.php");
        }
        $this->post = new Post();
        $this->post->init();
        
        echo "<h1>Dashboard</h1>";
        echo "<div id='sidebar'>";
            echo  "<div class='sidebarlink'><a href='index.php?add'>Add Post</a></div>";
            echo  "<div class='sidebarlink'><a href='index.php?manage'>Manage Posts</a></div>";
            echo  "<div class='sidebarlink'><a href='index.php?media'>Add Media</a></div>";
            echo  "<div class='sidebarlink'><a href='index.php?manage_media' target='_new'>Manage Media</a></div>";
            echo  "<div class='sidebarlink'><a href='index.php?generate' target='_new'>Generate Static Pages</a></div>";
            echo  "<div class='sidebarlink'><a href='".$SITE_BASE_URL."'>View Website</a></div>";
            echo  "<div class='sidebarlink'><a href='index.php?logout'>Logout</a></div>";
        echo "</div>";
        
        echo "<div id='main'>";
            if(isset($_GET['manage'])) {
                
                $this->renderManagePosts();
                
            } else if(isset($_GET['media'])) {
                
                $this->renderMedia();
                
            } else if(isset($_GET['edit'])) {
                
                $this->editPost($_GET['edit']);
                
            } else if(isset($_GET['manage_media'])) {
                
                $this->showMedia();
            
                
            } else if(isset($_GET['delete_media'])) {
                
                $this->deleteMedia($_GET['delete_media']);
            
                
            } else if(isset($_GET['generate'])) {
                
                $this->generate();
                
            } else if(isset($_GET['delete_post'])) {
                
                $this->post->deletePost($_GET['delete_post']);
                $this->renderManagePosts();
                
            } else if(isset($_POST['edit_post'])) {
                $id = $_POST['id'];
                $title = $_POST['title'];
                $category = $_POST['categoryinput'];
                $tags = $_POST['tags'];
                $content = $_POST['content'];
                $slug = $_POST['slug'];
                $page = $_POST['page'];
                $added_id = $this->post->editPost($title,$category,$tags,$content,$slug,$page,$id);
                $this->editPost($added_id);
            
             }  else if(isset($_POST['add_post'])) {
                $title = $_POST['title'];
                $categorys = $_POST['categoryselect'];
                $categoryi = $_POST['categoryinput'];
                if(intval($categorys)==0) {
                    $category = $categoryi;
                }
                else {
                    $category = $categorys;
                }
                $page = $_POST['page'];
                $tags = $_POST['tags'];
                $content = stripslashes($_POST['content']);
                $slug = $_POST['slug'];
                $added_id = $this->post->addPost($title,$category,$tags,$content,$_SESSION['username'],$slug,$page);
                $this->editPost($added_id);

            } else if(isset($_GET['logout'])) {
                session_destroy();
                header("Location:index.php");
            } 
            else {
                
                $this->renderAdd();
                
            }
        echo "</div>";
        
    }
    
    public function generate() {
        global $DEFAULT_TEMPLATE,$SITE_BASE_URL;
        $generator = new Generator();
        $generator->init();
        $generator->loadTemplate($SITE_BASE_URL.$DEFAULT_TEMPLATE);
        $generator->start();
        
        
    }
    
    public function renderAdd() {
        global $SITE_BASE_URL;
        ?>
        <script type="text/javascript" src="../js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
        <script type="text/javascript">
                tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		skin : "o2k7",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

		// Theme options
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "css/word.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		
	});
        </script>            
        <?
        $categories = $this->post->getCategories();
        
                ?>
                    <script type="text/javascript" src="/js/addtoSlug.js"></script>
                <?
                
                echo "<h2>Add Posts</h2>";
                
                echo "<form action='index.php' method='post'>";
                    echo "<table>";
                        echo "<tr>";
                            echo "<td>Title : </td>";
                            echo "<td><input type='text' value='' size='40' name='title' id='title' onkeyup='addToSlug();'></td>";
                        echo "</tr>";
                        echo "<tr>";
                            echo "<td>Slug  : </td>";
                            echo "<td>".$SITE_BASE_URL."<input type='text' value='' size='30'id='slug' name='slug'>.html</td>";
                        echo "</tr>";
                        echo "<tr>";
                            echo "<td>Category : </td>";
                            echo "<td>";
                                echo "<select name='categoryselect'>";
                                    echo "<option value='0'>--Enter New--</option>";
                                    foreach($categories as $category) {
                                        echo "<option value='".$category."'>".$category."</option>";
                                    }
                                echo "</select>";
                                echo "<input type='text' value='' name='categoryinput'>";
                            echo "</td>";
                        echo "</tr>";
                        echo "<tr>";
                            echo "<td colspan='2'>";
                                echo "<textarea rows='20' cols='80' name='content'></textarea>";
                            echo "</td>";
                        echo "</tr>";
                        echo "<tr>";
                            echo "<td>Tags : </td>";
                            echo "<td><input type='text' name='tags' size='40'></td>";
                        echo "</tr>";
                        echo "<tr>";
                            echo "<td>Make this a page? : </td>";
                            echo "<td>";
                                echo "<select name='page'>";
                                echo "<option value='0'>No</option>";
                                echo "<option value='1'>Yes</option>";
                            echo "</td>";
                        echo "</tr>";
                        echo "<tr>";
                            echo "<td colspan='2' align='right'>";
                                echo "<input type='submit' value='Add Post' name='add_post'>";
                            echo "</td>";
                        echo "</tr>";
                    echo "</table>";
                echo "</form>";
                
        
        
    }
    
    public function editPost($id) {
        ?>
        <script type="text/javascript" src="../js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
        <script type="text/javascript">
                tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		skin : "o2k7",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

		// Theme options
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "css/word.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		
	});
        </script>            
        <?
        $p = $this->post->retrievePost($id);
        
        echo "<h2>Edit Posts</h2>";

                echo "<form action='index.php' method='post'>";
                    echo "<input type='hidden' value='".$id."' name='id'>";
                    echo "<table>";
                        echo "<tr>";
                            echo "<td>Title : </td>";
                            echo "<td><input type='text' value='".$p->title."' size='40' name='title'></td>";
                        echo "</tr>";
                        echo "<tr>";
                            echo "<td>Slug  : </td>";
                            echo "<td>".$SITE_BASE_URL."<input type='text' value='".$p->slug."' size='30' name='slug'>.html</td>";
                        echo "</tr>";
                        echo "<tr>";
                            echo "<td>Category : </td>";
                            echo "<td>";
                                echo "<input type='text' value='".$p->category."' name='categoryinput'>";
                            echo "</td>";
                        echo "</tr>";
                        echo "<tr>";
                            echo "<td colspan='2'>";
                                echo "<textarea rows='20' cols='80' name='content'>".stripcslashes($p->content)."</textarea>";
                            echo "</td>";
                        echo "</tr>";
                        echo "<tr>";
                            echo "<td>Tags : </td>";
                            echo "<td><input type='text' name='tags' value='".$p->tags."' size='40'></td>";
                        echo "</tr>";
                        echo "<tr>";
                            echo "<td>Make this a page? : </td>";
                            echo "<td>";
                                echo "<select name='page'>";
                                echo "<option value='0'>No</option>";
                                echo "<option value='1' ";
                                if($p->page == 1) {
                                    
                                    echo "selected='selected'";
                                    
                                }
                                echo ">Yes</option>";
                            echo "</td>";
                        echo "</tr>";
                        echo "<tr>";
                            echo "<td colspan='2' align='right'>";
                                echo "<input type='submit' value='Edit This Post' name='edit_post'>";
                            echo "</td>";
                        echo "</tr>";
                    echo "</table>";
                echo "</form>";
          
    }
    
    public function renderManagePosts() {
        
        echo "<h2>Manage Posts</h2>";
        $simplexmlstream = $this->post->returnStream();
        $i = 0;
        foreach ($simplexmlstream->post as $post) {
            if($post->title=="") {
                $title = "No title";
            }
            else {
                $title = $post->title;
            }
            ?>
        <div id='singleLine'> <a href="?edit=<?php echo $post->id; ?>"><?php echo $title; ?></a> - <?php echo $post->date; ?> - <a href='index.php?delete_post=<?php echo $post->id; ?>'>Delete</a>
        <?
            if($post->page == 1) {
                ?>
                    <i>Page</i>
                <?
            }
        ?>
        </div>
            <?
            $i++;
        }
        
        if($i==0) {
            echo "<p>You don't have posts saved</p>";
        }
        
        
        
    }
    
    public function renderMedia() {
        global $UPLOAD_DIR,$SITE_BASE_URL;
         $full_dir_path = "../".$UPLOAD_DIR."/";
         $pre = $SITE_BASE_URL.$UPLOAD_DIR."/";
         if(!is_dir($full_dir_path)) {
                                echo "Created the directory at ".$full_dir_path."<br/>";
                                mkdir($full_dir_path); 
                            }
        
        echo "<h2>Manage Media</h2>";
        if(isset($_FILES['file'])) {
                        if ($_FILES["file"]["error"] > 0)
                        {
                            echo "Error: " . $_FILES["file"]["error"] . "<br />";
                        }
                        else
                        {
                 
                            $nArray = explode(".",$_FILES['file']["name"]);
                            $ext = $nArray[count($nArray)-1];
                            $file_name = $full_dir_path.md5(date('ymdHis').rand(1,500)).".".$ext;
                            @is_uploaded_file($_FILES['file']['tmp_name']) or die("Error while processing file");
                            move_uploaded_file($_FILES['file']['tmp_name'], $file_name); 
                            echo "<br/> Moved new file to ".$file_name;
                        }
        
        }
        ?>
        <form action="" method="post" enctype="multipart/form-data">
        <label for="file">Filename:</label>
        <input type="file" name="file" id="file" />
        <br />
        <input type="submit" name="submit" value="Submit" />
        </form>
        
        <?
        
        
    }
    
    public function deleteMedia($id) {
        
        global $UPLOAD_DIR,$SITE_BASE_URL;
         $full_dir_path = "../".$UPLOAD_DIR."/";
         $pre = $SITE_BASE_URL.$UPLOAD_DIR."/";
        $handle = opendir($full_dir_path);
      
        $i=0;
        while (false !== ($media = readdir($handle))) {
            
           if($i==$id) {
               unlink($full_dir_path.$media);
               echo "Deleted : ".$full_dir_path.$media;
           }
         $i+=1;   
        }
        $this->showMedia();
        
    }
    
    public function showMedia() {
        
        global $UPLOAD_DIR,$SITE_BASE_URL;
         $full_dir_path = "../".$UPLOAD_DIR."/";
         $pre = $SITE_BASE_URL.$UPLOAD_DIR."/";
        $handle = opendir($full_dir_path);
        
        echo "<h2>Media</h2>";
        echo "<table>";
        $i=0;
        while (false !== ($media = readdir($handle))) {
            if($media!="." && $media!="..") {
            ?>
                <tr><td><textarea rows="4" cols="60"><img src="<?php echo $pre.$media; ?>" title=""></textarea></td><td><?php $this->ifImageDisplay($pre,$media); ?></td><td><a href="index.php?delete_media=<?php echo $i; ?>">Delete</a></td></tr>
            <?
            }
            $i+=1;   
            
        }
        echo "</table>";
        
        if($i==2) {
            echo "You haven't uploaded any media!<br/>";
        }
        
        
    }
    
    
    public function ifImageDisplay($pre,$media) {
            
            $exts = array("png","jpg","jpeg","gif");
            
            $fArray = explode(".",$media);
            if(count($fArray)==2) {
                
                $ext = $fArray[1];
                if(in_array($ext,$exts)) {
                    
                    echo "<img src='".$pre.$media."' height='150' width='150' />";
                    
                }
            }
            
  

    }
}

?>
