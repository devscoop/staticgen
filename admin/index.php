<?php
    ob_start();
    session_start();
    require_once 'admin.inc.php';
?>
<html>
    <head>
        <title>Administrator - Dashboard</title>
        <link rel="stylesheet" type="text/css" href="../css/admin.css" />
    </head>
    <body>
        <?php
            $admin = new Admin();
            if(isset($_POST['username']) && isset($_POST['password'])) {
                        $admin->login($_POST['username'],$_POST['password']);
            } else if(isset($_SESSION['username']) && !empty($_SESSION['username'])) {
                
                         $admin->renderDashboard();
                
            } else {
        ?>
        <div id="loginpage">
            <form action="" method="post">
                <table>
                        <tr><td>Username : </td><td><input type="text" size="40" name="username"></td></tr>
                        <tr><td>Password : </td><td><input type="password" size="40" name="password"></td></tr>
                        <tr><td colspan="2"><input type="submit" value="Login"></td></tr>
                </table>
            </form>
        </div>
        <?
            }
        ?>
    </body>
</html>
<?php
    ob_flush();
?>